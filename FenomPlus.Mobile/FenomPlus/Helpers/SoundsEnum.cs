﻿using System;

namespace FenomPlus.Helpers
{
    public enum SoundsEnum
    {
        red_low,
        red_high,
        yellow_low,
        yellow_high,
        green_low,
        green_mid,
        green_high,
        test_failure,
        test_success
    }
}
