﻿using Xamarin.Forms.PlatformConfiguration;

namespace FenomPlus.ViewModels
{
    public class PairingViewModel : BaseViewModel
    {
        public PairingViewModel()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        override public void OnAppearing()
        {
            base.OnAppearing();
            Services.Navigation.GotoBluetoothSettings();
        }

        /// <summary>
        /// 
        /// </summary>
        override public void OnDisappearing()
        {
            base.OnDisappearing();
        }

        /// <summary>
        /// 
        /// </summary>
        override public void NewGlobalData()
        {
            base.NewGlobalData();
        }
    }
}
